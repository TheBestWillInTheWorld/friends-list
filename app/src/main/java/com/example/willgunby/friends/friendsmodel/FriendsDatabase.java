package com.example.willgunby.friends.friendsmodel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by willgunby on 07/04/15.
 */
class FriendsDatabase extends SQLiteOpenHelper {
    private static final String LOG_TAG = FriendsDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "friends.db";
    private static final int DATABASE_VERSION = 2;

    interface Tables {
        static final String FRIENDS = "friends";
    }

    public FriendsDatabase(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists "+ Tables.FRIENDS +" ("
        + BaseColumns._ID +" integer primary key autoincrement,"
        + FriendsContract.Friends.FRIENDS_NAME  +" TEXT NOT NULL,"
        + FriendsContract.Friends.FRIENDS_EMAIL +" TEXT NOT NULL,"
        + FriendsContract.Friends.FRIENDS_PHONE +" TEXT NOT NULL)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        int version = oldVersion;
        if (version==1) {
            //add some extra fields to DB, preserving existing data
            version = 2;
        }
        if (version != DATABASE_VERSION) {
            db.execSQL("drop table if exists "+ Tables.FRIENDS);
            onCreate(db);
        }
    }


    public static void deleteDatabase(Context context) {
        context.deleteDatabase(DATABASE_NAME);
    }


}
