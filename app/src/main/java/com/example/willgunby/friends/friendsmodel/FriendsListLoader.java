package com.example.willgunby.friends.friendsmodel;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by willgunby on 11/04/15.
 */
public class FriendsListLoader extends AsyncTaskLoader<List<Friend>> {
    String LOG_TAG = FriendsListLoader.class.getSimpleName();
    private List<Friend> mFriends;
    private final ContentResolver mContentResolver;
    private Cursor mCursor;

    public FriendsListLoader(Context context, ContentResolver contentResolver) {
        super(context);
        mContentResolver = contentResolver;
    }

    @Override
    public List<Friend> loadInBackground() {
        return getFriends(null);
    }

    List<Friend> getFriends(String nameSearch) {
        String[] projection = { FriendsContract.Friends._ID,
                FriendsContract.Friends.FRIENDS_NAME,
                FriendsContract.Friends.FRIENDS_PHONE,
                FriendsContract.Friends.FRIENDS_EMAIL};
        ArrayList<Friend> entries = new ArrayList<>();

        String   nameClause = null;
        String[] args       = null;
        if (nameSearch!=null && !nameSearch.isEmpty()) {
            nameClause=FriendsContract.Friends.FRIENDS_NAME + " like ? ";
            args = new String[]{nameSearch +'%'};
        }
        mCursor = mContentResolver.query(FriendsContract.Friends.CONTENT_URI, projection, nameClause, args, FriendsContract.Friends.FRIENDS_NAME);

        if (mCursor!=null){
            if (mCursor.moveToFirst()) {
                do {
                    int _id      = mCursor.getInt   (mCursor.getColumnIndex(FriendsContract.Friends.FRIENDS_ID));
                    String name  = mCursor.getString(mCursor.getColumnIndex(FriendsContract.Friends.FRIENDS_NAME));
                    String phone = mCursor.getString(mCursor.getColumnIndex(FriendsContract.Friends.FRIENDS_PHONE));
                    String email = mCursor.getString(mCursor.getColumnIndex(FriendsContract.Friends.FRIENDS_EMAIL));
                    Friend friend = new Friend(_id, name, phone, email);
                    entries.add(friend);
                } while (mCursor.moveToNext()) ;
            }
        }

        return entries;
    }

    @Override
    public void deliverResult(List<Friend> newData) {
        if (isReset() && newData != null) mCursor.close();
        List<Friend> oldFriendList = mFriends;
        mFriends = newData;

        if (mFriends == null || mFriends.size() == 0) Log.d(LOG_TAG, "+++++++++++ No data returned");
        if (isStarted()) super.deliverResult(newData);
        if (oldFriendList!=null && oldFriendList!=newData) mCursor.close();
    }

    @Override
    protected void onStartLoading() {
        if (mFriends!=null) deliverResult(mFriends);
        if (takeContentChanged() || mFriends==null) forceLoad();
        //super.onStartLoading();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
        //super.onStopLoading();
    }

    @Override
    protected void onReset() {
        onStopLoading();
        if (mCursor!=null) mCursor.close();
        mFriends = null;
        //super.onReset();
    }

    @Override
    public void onCanceled(List<Friend> data) {
        super.onCanceled(data);
        if (mCursor!=null) mCursor.close();
    }

}
