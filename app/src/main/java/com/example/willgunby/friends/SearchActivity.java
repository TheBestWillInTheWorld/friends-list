package com.example.willgunby.friends;

import android.content.ContentResolver;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.willgunby.friends.friendsmodel.Friend;
import com.example.willgunby.friends.friendsmodel.FriendsSearchListLoader;

import java.util.List;

/**
 * Created by willgunby on 11/04/15.
 */
public class SearchActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<List<Friend>>{
    private static final String LOG_TAG = Friend.class.getSimpleName();

    private FriendCustomAdapter mFriendCustomAdapter;
    private static int LOADER_ID = 2;
    private ContentResolver mContentResolver;
    private EditText     txtSearchName;
    private String       mMatchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);
        ListView lstSearchResults = (ListView) findViewById(R.id.lstSearchResults);
        txtSearchName    = (EditText) findViewById(R.id.txtSearchName);
        Button btnSearch = (Button) findViewById(R.id.btnSearch);

        mContentResolver = getContentResolver();
        mFriendCustomAdapter = new FriendCustomAdapter(SearchActivity.this, getSupportFragmentManager());
        lstSearchResults.setAdapter(mFriendCustomAdapter);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMatchText = txtSearchName.getText().toString();
                getSupportLoaderManager().initLoader(LOADER_ID++, null, SearchActivity.this);
            }
        });
    }

    @Override
    public Loader<List<Friend>> onCreateLoader(int i, Bundle bundle) {
        return new FriendsSearchListLoader(SearchActivity.this, this.mContentResolver, mMatchText);
    }

    @Override
    public void onLoadFinished(Loader<List<Friend>> listLoader, List<Friend> friends) {
        mFriendCustomAdapter.setData(friends);
    }

    @Override
    public void onLoaderReset(Loader<List<Friend>> listLoader) {
        mFriendCustomAdapter.setData(null);
    }
}
