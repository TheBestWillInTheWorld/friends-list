package com.example.willgunby.friends.friendsmodel;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by willgunby on 07/04/15.
 */
public class FriendsContract {

    interface FriendsColumns {
        public static final String FRIENDS_ID    = "_id";
        public static final String FRIENDS_NAME  = "friends_name";
        public static final String FRIENDS_EMAIL = "friends_email";
        public static final String FRIENDS_PHONE = "friends_phone";
    }
    public static final  String CONTENT_AUTHORITY = "com.example.willgunby.friends.provider";
    public static final  Uri    BASE_CONTENT_URI  = Uri.parse("content://"+ CONTENT_AUTHORITY);
    private static final String PATH_FRIENDS      = "friends";

    public static final String[] TOP_LEVEL_PATHS = {
            PATH_FRIENDS
    };

    public static class Friends implements FriendsColumns, BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendEncodedPath(PATH_FRIENDS).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vdn."+ CONTENT_AUTHORITY + "."+ PATH_FRIENDS;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vdn."+ CONTENT_AUTHORITY + "."+ PATH_FRIENDS;

        public static Uri buildFriendUri(String friendId){
            return CONTENT_URI.buildUpon().appendEncodedPath(friendId).build();
        }

        public static String getFriendId(Uri uri){
            return uri.getPathSegments().get(1);
        }

    }
}
