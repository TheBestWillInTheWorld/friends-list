package com.example.willgunby.friends;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.willgunby.friends.friendsmodel.Friend;

/**
 * Created by willgunby on 11/04/15.
 */
public class EditActivity extends _AddEditBaseActivity {
    private static final String LOG_TAG = EditActivity.class.getSimpleName();
    private Friend mFriend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFriend = Friend.fromIntent(getIntent());
        txtName.setText (mFriend.getName() );
        txtPhone.setText(mFriend.getPhone());
        txtEmail.setText(mFriend.getEmail());

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    mFriend.setName ( txtName.getText().toString());
                    mFriend.setPhone(txtPhone.getText().toString());
                    mFriend.setEmail(txtEmail.getText().toString());
                    mFriend.save(mContentResolver);
                    Intent intent = new Intent(EditActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please ensure you have entered some valid data", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public Boolean isDirty(){
        return ! txtName.getText().toString().equals(mFriend.getName() )||
               !txtPhone.getText().toString().equals(mFriend.getPhone())||
               !txtEmail.getText().toString().equals(mFriend.getEmail());
    }

}
