package com.example.willgunby.friends.friendsmodel;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.example.willgunby.friends.FriendsDialog;

/**
 * Created by willgunby on 11/04/15.
 */
public class Friend {
    private static final String LOG_TAG = Friend.class.getSimpleName();
    private static final long serialVersionUID = 0L;

    private int _id;
    private String name;
    private String phone;
    private String email;

    public Friend(String name, String phone, String email) {
        this._id = -1;
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public Friend(int id, String name, String phone, String email) {
        this._id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int save(ContentResolver contentResolver){
        ContentValues contentValues = new ContentValues();
        contentValues.put(FriendsContract.Friends.FRIENDS_NAME,  name);
        contentValues.put(FriendsContract.Friends.FRIENDS_PHONE, phone);
        contentValues.put(FriendsContract.Friends.FRIENDS_EMAIL, email);
        if (_id!=-1) {
            //existing record
            Uri uri = FriendsContract.Friends.buildFriendUri(String.valueOf(_id));
            int recordsUpdated = contentResolver.update(uri, contentValues, null, null);
            Log.d(LOG_TAG, recordsUpdated +" records updated for id " + _id);
            return recordsUpdated;
        } else {
            Uri returned = contentResolver.insert(FriendsContract.Friends.CONTENT_URI, contentValues);
            Log.d(LOG_TAG, "record id returned is " + returned.toString());
            return 1;
        }
    }

    public static Friend fromIntent(Intent receivedIntent){
        final int _id      = receivedIntent.getIntExtra(FriendsContract.Friends.FRIENDS_ID, -1);
        final String name  = receivedIntent.getStringExtra(FriendsContract.Friends.FRIENDS_NAME);
        final String phone = receivedIntent.getStringExtra(FriendsContract.Friends.FRIENDS_PHONE);
        final String email = receivedIntent.getStringExtra(FriendsContract.Friends.FRIENDS_EMAIL);
        return new Friend(_id, name, phone, email);
    }

    public Intent toIntent(Context context, Class target){
        Intent intent = new Intent(context, target);
        intent.putExtra(FriendsContract.Friends.FRIENDS_ID   ,_id);
        intent.putExtra(FriendsContract.Friends.FRIENDS_NAME ,name);
        intent.putExtra(FriendsContract.Friends.FRIENDS_PHONE,phone);
        intent.putExtra(FriendsContract.Friends.FRIENDS_EMAIL,email);
        return intent;
    }

    public Bundle toBundle(){
        Bundle args = new Bundle();
        args.putString(FriendsDialog.DIALOG_TYPE, FriendsDialog.DELETE_RECORD);
        args.putInt   (FriendsContract.Friends.FRIENDS_ID,_id);
        args.putString(FriendsContract.Friends.FRIENDS_NAME ,name);
        args.putString(FriendsContract.Friends.FRIENDS_PHONE,phone);
        args.putString(FriendsContract.Friends.FRIENDS_EMAIL,email);
        return args;
    }

}
