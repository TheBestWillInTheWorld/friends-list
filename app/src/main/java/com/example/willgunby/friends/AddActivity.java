package com.example.willgunby.friends;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.willgunby.friends.friendsmodel.Friend;

/**
 * Created by willgunby on 11/04/15.
 */
public class AddActivity extends _AddEditBaseActivity {
    private static final String LOG_TAG = AddActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    Friend friend = new Friend( txtName.getText().toString(),
                                                txtPhone.getText().toString(),
                                                txtEmail.getText().toString());
                    friend.save(mContentResolver);
                    Intent intent = new Intent(AddActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Please ensure you have entered some valid data", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public Boolean isDirty(){
        return txtName.getText().toString().length() > 0 ||
                txtPhone.getText().toString().length() > 0 ||
                txtEmail.getText().toString().length() > 0;
    }

}
