package com.example.willgunby.friends;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.willgunby.friends.friendsmodel.FriendsContract;

/**
 * Created by willgunby on 11/04/15.
 */
public class FriendsDialog extends DialogFragment {
    private static final String LOG_TAG = FriendsDialog.class.getSimpleName();
    public static final String DIALOG_TYPE      = "command";
    public static final String DELETE_RECORD    = "deleteRecord";
    public static final String DELETE_DATABASE  = "deleteDatabase";
    public static final String CONFIRM_EXIT     = "confirmExit";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        final View view = layoutInflater.inflate(R.layout.friend_dialog, null);

        if (getArguments()==null) {
            Log.e(LOG_TAG, "getArguments() returned null");
        }

        String command = getArguments().getString(DIALOG_TYPE);

        switch (command) {
            case DELETE_RECORD: {
                final int _id = getArguments().getInt(FriendsContract.Friends.FRIENDS_ID);
                String name = getArguments().getString(FriendsContract.Friends.FRIENDS_NAME);
                Uri uri = FriendsContract.Friends.buildFriendUri(String.valueOf(_id));
                String msg = "Are you sure you want to delete " + name + " from your friends list?";
                deleteWithConfirm(builder, view, msg, uri, name + " deleted");

                break;
            }
            case DELETE_DATABASE: {
                String msg = "Are you sure you want to delete your ENTIRE friends list?";
                final Uri uri = FriendsContract.Friends.CONTENT_URI;
                deleteWithConfirm(builder, view, msg, uri, "Friends list cleared!");
                break;
            }
            case CONFIRM_EXIT: {
                String msg = "If you go back now, you'll lose what you've entered.  Are you sure you want to do this?";
                exitWithConfirm(builder, view, msg);
                break;
            }
            default:
                Log.d(LOG_TAG, "Invalid command '" + command + "' received as parameter");
                break;
        }


        return builder.create();
    }

    private void deleteWithConfirm(final AlertDialog.Builder builder, final View view, final String msg, final Uri uri, final String completionMsg) {
        TextView popupMessage = (TextView) view.findViewById(R.id.popup_message);
        popupMessage.setText(msg);
        /*
            note: with more complex dialog logic, this would need refactoring using interfaces
            from a model class that pass control back to the called when input is needed
            to ensure the workflow is not entangled with the GUI implementation
         */
        builder.setView(view)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ContentResolver contentResolver = getActivity().getContentResolver();
                contentResolver.delete(uri, null, null);
                Toast.makeText(getActivity().getApplicationContext(), completionMsg, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        })
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    private void exitWithConfirm(AlertDialog.Builder builder, View view, String msg) {
        TextView popupMessage = (TextView) view.findViewById(R.id.popup_message);
        popupMessage.setText(msg);
        builder.setView(view)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
            }
        })
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

}
