package com.example.willgunby.friends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.willgunby.friends.friendsmodel.Friend;

import java.util.List;

/**
 * Created by willgunby on 11/04/15.
 */
public  class FriendCustomAdapter extends ArrayAdapter {
    private final LayoutInflater mLayoutInflater;
    private static FragmentManager sFragmentManager;

    public FriendCustomAdapter(Context context, FragmentManager fragmentManager) {
        super(context, android.R.layout.simple_list_item_2);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sFragmentManager = fragmentManager;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view;
        if (convertView==null)  view = mLayoutInflater.inflate(R.layout.custom_friend, parent, false);
        else                    view = convertView;

        final Friend friend = (Friend) getItem(position);

        ((TextView) view.findViewById(R.id.friend_name )).setText(friend.getName() );
        ((TextView) view.findViewById(R.id.friend_phone)).setText(friend.getPhone());
        ((TextView) view.findViewById(R.id.friend_email)).setText(friend.getEmail());

        Button btnEdit = (Button) view.findViewById(R.id.btnEdit);
        Button btnDelete = (Button) view.findViewById(R.id.btnDelete);


        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = friend.toIntent(getContext(),EditActivity.class);
                getContext().startActivity(intent);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FriendsDialog friendsDialog = new FriendsDialog();
                Bundle args = friend.toBundle();
                args.putString(FriendsDialog.DIALOG_TYPE, FriendsDialog.DELETE_RECORD);
                friendsDialog.setArguments(args);
                friendsDialog.show(sFragmentManager, "delete-record");
            }
        });

        return view;
    }

    public void setData(List<Friend> data) {
        clear();
        if (data!=null) {
            for(Friend friend : data){
                add(friend);
            }
        }
    }
}
