package com.example.willgunby.friends.friendsmodel;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;
import android.util.Log;

import java.util.Arrays;

/**
 * Created by willgunby on 07/04/15.
 */
public class FriendsProvider extends ContentProvider {
    private FriendsDatabase mOpenHelper;

    private static final String LOG_TAG = FriendsProvider.class.getSimpleName();
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int FRIENDS = 100;
    private static final int FRIENDS_ID = 101;


    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = FriendsContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, "friends", FRIENDS);
        matcher.addURI(authority, "friends/*", FRIENDS_ID);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new FriendsDatabase(getContext());
        return true;
    }

    private void deleteDatabase(){
        mOpenHelper.close();
        FriendsDatabase.deleteDatabase(getContext());
        mOpenHelper = new FriendsDatabase(getContext());
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match){
            case FRIENDS:
                return FriendsContract.Friends.CONTENT_TYPE;
            case FRIENDS_ID:
                return FriendsContract.Friends.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown Uri: "+ uri);
        }
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        final int match = sUriMatcher.match(uri);

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(FriendsDatabase.Tables.FRIENDS);

        switch (match){
            case FRIENDS:
                //do nothing
                break;
            case FRIENDS_ID:
                String id = FriendsContract.Friends.getFriendId(uri);
                queryBuilder.appendWhere(BaseColumns._ID+"="+id);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: "+ uri);
        }

        return queryBuilder.query(db,projection,selection,selectionArgs,null,null,sortOrder);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.v(LOG_TAG,"insert(uri="+uri+", values="+values);
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);

        switch (match){
            case FRIENDS:
                long recordId = db.insertOrThrow(FriendsDatabase.Tables.FRIENDS, null,values);
                return FriendsContract.Friends.buildFriendUri(String.valueOf(recordId));
            default:
                throw new IllegalArgumentException("Unknown Uri: "+ uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.v(LOG_TAG,"update(uri="+uri+", values="+values+", selection="+selection+", selectionArgs="+ Arrays.toString(selectionArgs));
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        String selectionCriteria = selection;

        switch (match){
            case FRIENDS:
                //do nothing
                break;
            case FRIENDS_ID:
                String id = FriendsContract.Friends.getFriendId(uri);
                selectionCriteria = BaseColumns._ID+"="+id
                            +(!TextUtils.isEmpty(selection)? " AND (" + selection + ")" : "");
                break;

            default:
                throw new IllegalArgumentException("Unknown Uri: "+ uri);
        }
        return db.update(FriendsDatabase.Tables.FRIENDS, values, selectionCriteria, selectionArgs);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.v(LOG_TAG,"delete(uri="+uri+", selection="+selection+", selectionArgs="+ Arrays.toString(selectionArgs));

        /*
            ideally the confirmation should be triggered from this level to make sure the
            confirmation is always relevant to the action being undertaken.
            during testing, there was a bug in the delete code that resulted in a DB wipe rather
            than single record delete - the confirmation stated it was for a single record.
         */
        if (uri.equals(FriendsContract.Friends.CONTENT_URI)){
            Log.d(LOG_TAG,"deleting database! (uri="+uri+", selection="+selection+", selectionArgs="+ Arrays.toString(selectionArgs));
            deleteDatabase();
            return 0;
        }

        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);

        switch (match){
            case FRIENDS_ID:
                String id = FriendsContract.Friends.getFriendId(uri);
                selection = BaseColumns._ID+"="+id
                        +(!TextUtils.isEmpty(selection)? " AND (" + selection + ")" : "");
                Log.d(LOG_TAG,"deleting record (uri="+uri+", selection="+selection+", selectionArgs="+ Arrays.toString(selectionArgs));
                return db.delete(FriendsDatabase.Tables.FRIENDS, selection, selectionArgs);

            default:
                throw new IllegalArgumentException("Unknown Uri: "+ uri);
        }
    }
}
