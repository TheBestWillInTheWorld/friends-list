package com.example.willgunby.friends.friendsmodel;

import android.content.ContentResolver;

import com.example.willgunby.friends.SearchActivity;

import java.util.List;

/**
 * Created by willgunby on 12/04/15.
 */
public class FriendsSearchListLoader extends FriendsListLoader {
    private final String mNameSearch;

    public FriendsSearchListLoader(SearchActivity searchActivity, ContentResolver contentResolver, String nameSearch) {
        super(searchActivity,contentResolver);
        mNameSearch = nameSearch;
        LOG_TAG = FriendsSearchListLoader.class.getSimpleName();
    }

    @Override
    public List<Friend> loadInBackground() {
        return getFriends(mNameSearch);
    }
}
