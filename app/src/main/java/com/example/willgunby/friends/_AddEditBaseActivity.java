package com.example.willgunby.friends;

import android.app.ActionBar;
import android.content.ContentResolver;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by willgunby on 11/04/15.
 */
abstract class _AddEditBaseActivity extends FragmentActivity {

    TextView txtName ;
    TextView txtEmail;
    TextView txtPhone;
    Button btnSave;
    ContentResolver mContentResolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_edit);
        ActionBar bar=getActionBar();
        if (bar!=null) bar.setDisplayHomeAsUpEnabled(true);

        txtName = (TextView) findViewById(R.id.txtName );
        txtPhone = (TextView) findViewById(R.id.txtPhone);
        txtEmail = (TextView) findViewById(R.id.txtEmail);

        mContentResolver = getContentResolver();

        btnSave = (Button) findViewById(R.id.btnSave);
    }

    boolean isValid() {
        return !(txtName.getText().toString().length() == 0 ||
                txtPhone.getText().toString().length() == 0 ||
                txtEmail.getText().toString().length() == 0);
    }

    protected abstract Boolean isDirty();

    @Override
    public void onBackPressed() {
        if (isDirty()) {
            FriendsDialog friendsDialog = new FriendsDialog();
            Bundle args = new Bundle();
            args.putString(FriendsDialog.DIALOG_TYPE, FriendsDialog.CONFIRM_EXIT);
            friendsDialog.setArguments(args);
            friendsDialog.show(getSupportFragmentManager(), "confirm-exit");
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //this is effectively the up one level button
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                break;
        }
        return true;
        //return super.onOptionsItemSelected(item);
    }
}
